import Style from "../assets/Line-Up.module.css";
import React from "react";

class Induk extends React.Component {
    render() {
        return <div className={Style.body}>
            <div className={Style.card}>
                <h1>Title - Card 1</h1>
                <p>Medium length description. Let's add a few more words here.</p>
                <div className={Style.visual}></div>
            </div>
            <div className={Style.card}>
                <h1>Title - Card 2</h1>
                <p>Long Description. Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                 Sed est error repellat veritatis.</p>
                 <div className={Style.visual}></div>
            </div>
            <div className={Style.card}>
                <h1>Title - Card 3</h1>
                <p>Short Description.</p>
                <div className={Style.visual}></div>
            </div>
            </div>
    }
}

export default Induk;