import Style from "../assets/Respect-Aspect.module.css";
import React from "react";

class Induk extends React.Component {
    render() {
        return <div className={Style.body}>
            <div className={Style.card}>
                <div className={Style.h1}>Title Here</div>
                <div className={Style.visual}></div>
                <p>Descriptive Text. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>   
            </div>
            </div>
    }
}

export default Induk;