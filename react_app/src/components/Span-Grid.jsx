import styles from "../assets/Span-Grid.module.css";
import React from "react";

class Induk extends React.Component {
    render() {
        return <div className={styles.body}>
            <br></br>
            <div className={`${styles.span12} ${styles.div}`}>Span 12</div>           
            <div className={`${styles.span6} ${styles.div}`}>Span 6</div>
            <div className={`${styles.span4} ${styles.div}`}>Span 4</div>
            <div className={`${styles.span2} ${styles.div}`}>Span 2</div>
            </div>
    }
}

export default Induk;