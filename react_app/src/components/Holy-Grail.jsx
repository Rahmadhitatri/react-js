import Style from "../assets/Holy-Grail.module.css";
import React from "react";

class Induk extends React.Component {
    render() {
        return <div className={Style.body}>
            <div className={Style.header}>Header.com</div>
            <div className={Style.leftsidebar}>Left Sidebar</div>           
            <div className={Style.main}></div>
            <div className={Style.rightsidebar}>Right Sidebar</div>
            <div className={Style.footer}>Footer Content — Header.com 2020
            </div>
        </div>
    }
}

export default Induk;