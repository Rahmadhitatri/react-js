import React from "react";
import logo from './logo.svg';
import './App.css';
import Home from './home.js';
import SuperCenter from "./components/Super-Center";
import DeconPancake from "./components/Deconstructed-Pancake";
import SideBar from "./components/Sidebar";
import PancakeStack from "./components/Pancake-Stack"
import HolyGrail from "./components/Holy-Grail"
import SpanGrid from "./components/Span-Grid"
import Ram from "./components/RAM"
import LineUp from "./components/Line-Up"
import Clamping from "./components/Clamping"
import RespectAspect from "./components/Respect-Aspect"

function App() {
  return (
  <div className="App">
    <SuperCenter/>
    <DeconPancake/>
    <SideBar/>
    <PancakeStack/>
    <HolyGrail/>
    <SpanGrid/>
    <Ram/>
    <LineUp/>
    <Clamping/>
    <RespectAspect/>
  </div>
  );
}

export default App;